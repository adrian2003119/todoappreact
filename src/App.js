import './App.css';
import React, { Component } from 'react';
import { AiFillCloseCircle } from 'react-icons/all';
export default class ToDoList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userInput: '',
      list: [],
    };
  }
  changeUserInput(input) {
    this.setState({
      userInput: input,
    });
  }
  taskId = 0;
  addToList(input) {
    let listArray = this.state.list;
    listArray.push({ todoText: input, todoId: this.taskId });
    this.taskId += 1;
    this.setState({
      list: listArray,
      userInput: '',
    });
  }
  removeFromList(removingId) {
    let removingList = this.state.list;
    for (let i = 0; i < removingList.length; i += 1) {
      if (removingId === removingList[i].todoId) {
        removingList.splice(i, 1);
      }
    }
    this.setState({ list: removingList, userInput: '' });
  }
  completedTaskChangeColor(id) {
    if (document.getElementById(id).style.backgroundColor == 'green') {
      document.getElementById(id).style.backgroundColor = 'gray';
    } else {
      document.getElementById(id).style.backgroundColor = 'green';
    }
  }
  render() {
    return (
      <div className='to-do-list-main'>
        <h1>Todo APP Adrian Sójka</h1>
        <h1>Write task down below</h1>
        <input
          value={this.state.userInput}
          type='text'
          onChange={(e) => this.changeUserInput(e.target.value)}
          placeholder='Write Task Here'
        ></input>
        <button onClick={() => this.addToList(this.state.userInput)}>+</button>
        <ul>
          {this.state.list.map((val) => (
            <li
              onClick={() => this.completedTaskChangeColor(val.todoId)}
              id={val.todoId}
              key={val.todoId}
            >
              {val.todoText}
              <AiFillCloseCircle
                onClick={() => this.removeFromList(val.todoId)}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
